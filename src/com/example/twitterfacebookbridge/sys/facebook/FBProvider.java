package com.example.twitterfacebookbridge.sys.facebook;

/*
 * part of code been taken from official facebook example app
 * 
 * @author vofan
 * */

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.twitterfacebookbridge.R;
import com.example.twitterfacebookbridge.R.string;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.Request.Callback;
import com.facebook.android.Facebook;
import com.facebook.model.GraphUser;

public class FBProvider {
	static Facebook fb;

	private static final String TAG = FBProvider.class.getSimpleName();

	// Declare the UiLifecycleHelper for Facebook session management
	private static UiLifecycleHelper fbUiLifecycleHelper;

	private static Activity mainActivity;

	@SuppressWarnings("deprecation")
	public static void Init(Activity activity, Bundle savedInstanceState) {
		mainActivity = activity;
		
		// Instantiate the fbUiLifecycleHelper and call onCreate() on it
		fbUiLifecycleHelper = new UiLifecycleHelper(mainActivity,
				new Session.StatusCallback() {
					@Override
					public void call(Session session, SessionState state,
							Exception exception) {
						// Add code here to accommodate session changes
						Log.d(TAG, "Session state changed: " + state
								+ "  permissions: " + session.getPermissions());

						if (exception != null)
							Log.e(TAG, "UiLifecycleHelper exception: "
									+ exception.getMessage());

						if (state.isOpened()) {
							if (state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
								// Only callback if the opened token has
								// been updated - i.e. the user
								// has provided write permissions
								tokenUpdated();

								Log.d(TAG,
										"Session state changed: " + state
												+ "  permissions: "
												+ session.getPermissions());
								Log.d(TAG, "Session state changed2222: "
										+ state
										+ "  permissions: "
										+ Session.getActiveSession()
												.getPermissions());

							}
						}
					}
				});
		fbUiLifecycleHelper.onCreate(savedInstanceState);

		String app_id = mainActivity.getString(R.string.facebook_app_id);
		fb = new Facebook(app_id);
	}

	// Called when the session state has changed
	static void tokenUpdated() {
		// Optional place to take action after the token has been updated
		// (typically after a new permission has
		// been granted)
	}

	public static void onActivityResult(int requestCode, int resultCode,
			Intent data) {
		fbUiLifecycleHelper.onActivityResult(requestCode, resultCode, data);
	}

	public static void onResume() {
		fbUiLifecycleHelper.onResume();
	}

	public static void onPause() {
		fbUiLifecycleHelper.onPause();
	}

	public static void onSaveInstanceState(Bundle outState) {
		fbUiLifecycleHelper.onSaveInstanceState(outState);
	}

	public static void onDestroy() {
		fbUiLifecycleHelper.onDestroy();
	}
	
	private static boolean sessionIsReady()
	{
		if (Session.getActiveSession() != null
				&& Session.getActiveSession().isOpened()) {
			return true;
		} else {

			Session session = Session.getActiveSession();
			if (!session.isOpened() && !session.isClosed()) {

				session.openForRead(new Session.OpenRequest(mainActivity)
					//  .setPermissions(permissions)
						.setCallback(mFacebookCallback));
			} else {
				Session.openActiveSession(mainActivity, true, mFacebookCallback);
			}
			return false;
		}
	}
	
	private static Session.StatusCallback mFacebookCallback = new Session.StatusCallback() {
		@Override
		public void call(final Session session, final SessionState state,
				final Exception exception) {

			if (state.isOpened()) {
				
				String facebookToken = session.getAccessToken();
				Log.i("MainActivityFaceBook", facebookToken);
				Request.newMeRequest(session, new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, com.facebook.Response response) {
						Toast.makeText(mainActivity, "session opened. you can try to publish again", Toast.LENGTH_SHORT).show();
					}
				}).executeAsync();
			}
		}
	};

	public static void postOnWall(Uri imgSourceUri, String msg) {
		if(sessionIsReady()){
			try {
				Bitmap bi = BitmapFactory.decodeStream(mainActivity
						.getContentResolver().openInputStream(imgSourceUri));
	
				Request photoRequest = Request.newUploadPhotoRequest(
						Session.getActiveSession(), bi, new Callback() {
							@Override
							public void onCompleted(Response response) {
								if (response.getError() != null) {
									Toast.makeText(mainActivity, " error!!! " + response.getError().getErrorMessage(),Toast.LENGTH_SHORT).show();
								} else {
									Toast.makeText(mainActivity, "post appproved by FB", Toast.LENGTH_SHORT).show();
								}
							}
						});
	
				Bundle params = photoRequest.getParameters();
				params.putString("message", msg);
				photoRequest.executeAsync();
				Toast.makeText(mainActivity, "post stated", Toast.LENGTH_SHORT)
						.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		else
		{
			Toast.makeText(mainActivity, "login first", Toast.LENGTH_SHORT).show();
		}
	}
}
