package com.example.twitterfacebookbridge.sys.DB;

/*
 * @author vofan
 * */

import com.example.twitterfacebookbridge.sys.twitter.Twit;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {

	static SQLiteDatabase db = null;
	
	public static void init(Context context)
	{
		DBHelper dbHelper = new DBHelper(context);
		db = dbHelper.getWritableDatabase();
		dbHelper.onUpgrade(db, 1, 2); //clear table
	}
	
	public static void insertTwit(Twit twit)
	{
		ContentValues values = new ContentValues();
		values.put(DBHelper.columns[1], twit.body);
		values.put(DBHelper.columns[2], twit.owner);
		values.put(DBHelper.columns[3], twit.date.toGMTString());
		db.insert(DBHelper.TABLE_NAME, null, values); //if returns -1, that's means that we are trying to add already contained value. ignore tis
		values.clear();
	}
	
	public static Cursor readTwitsFromDB() throws Exception
	{
		if(db == null)
		{
			throw(new Exception("DB is not inited"));
		}
		//TODO: read just last 10 twits
		return db.rawQuery("SELECT * FROM " + DBHelper.TABLE_NAME + " ORDER BY " + DBHelper.columns[0], null);
	}
	
	public static void dropTable()
	{
		db.execSQL("DROP TABLE IF EXISTS '" + DBHelper.TABLE_NAME + "'");
	}
}
