package com.example.twitterfacebookbridge.sys.DB;

/*
 * @author vofan
 * */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper{

	public final static String[] columns = { "_id", "body", "owner", "date" };

	final public static String TABLE_NAME = "twits_table";
	final private static String CREATE_CMD = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
			+ columns[0] + " INTEGER PRIMARY KEY AUTOINCREMENT,"
			+ columns[1] + " TEXT NOT NULL,"
			+ columns[2] + " TEXT NOT NULL,"
			+ columns[3] + " TEXT UNIQUE NOT NULL"//" DATETIME NOT NULL "
			+");";
	
	public DBHelper(Context context) {
		super(context, TABLE_NAME, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_CMD);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
}
