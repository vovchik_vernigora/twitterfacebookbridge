package com.example.twitterfacebookbridge.sys.twitter;

/*
 * sources get from https://github.com/ddewaele/AndroidTwitterGoogleApiJavaClient
 * */


public interface CredentialStore {
	String[] read();

	void write(String[] response);

	void clearCredentials();
}