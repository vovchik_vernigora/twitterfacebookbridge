package com.example.twitterfacebookbridge.sys.twitter;

/*
 * sources get from https://github.com/ddewaele/AndroidTwitterGoogleApiJavaClient
 * */

import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import android.content.SharedPreferences;

public class TwitterUtils {
	public static boolean isAuthenticated(SharedPreferences prefs) {
		String[] tokens = new SharedPreferencesCredentialStore(prefs).read();
		AccessToken a = new AccessToken(tokens[0], tokens[1]);
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(Constants.API_KEY, Constants.API_SECRET);
		twitter.setOAuthAccessToken(a);
		try {
			twitter.getAccountSettings();
			return true;
		} catch (TwitterException e) {
			return false;
		}
	}

	public static ResponseList<Status> getHomeTimeline(SharedPreferences prefs)
			throws Exception {
		String[] tokens = new SharedPreferencesCredentialStore(prefs).read();
		AccessToken a = new AccessToken(tokens[0], tokens[1]);
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(Constants.API_KEY, Constants.API_SECRET);
		twitter.setOAuthAccessToken(a);
		ResponseList<Status> homeTimeline = twitter.getHomeTimeline();
		return homeTimeline;
	}

	public static ResponseList<Status> getUserTimeline(SharedPreferences prefs, String userName, int count)
			throws Exception {
		String[] tokens = new SharedPreferencesCredentialStore(prefs).read();
		AccessToken a = new AccessToken(tokens[0], tokens[1]);
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(Constants.API_KEY, Constants.API_SECRET);
		twitter.setOAuthAccessToken(a);
		ResponseList<Status> userHomeTimeline = twitter.getUserTimeline(userName/*, new Paging(0, count)*/); // last 10 twits
		return userHomeTimeline;
	}
	
	public static void sendTweet(SharedPreferences prefs, String msg)
			throws Exception {
		String[] tokens = new SharedPreferencesCredentialStore(prefs).read();
		AccessToken a = new AccessToken(tokens[0], tokens[1]);
		Twitter twitter = new TwitterFactory().getInstance();
		twitter.setOAuthConsumer(Constants.API_KEY, Constants.API_SECRET);
		twitter.setOAuthAccessToken(a);
		twitter.updateStatus(msg);
	}
}