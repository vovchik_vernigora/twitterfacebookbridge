package com.example.twitterfacebookbridge.sys.twitter;

/*
 * @author vofan
 * */

import java.util.Date;

import android.database.Cursor;

public class Twit {

	public String body;
	public String owner;
	public Date date;
	
	public Twit( String body, String owner, Date date)
	{
		this.body = body;
		this.owner = owner;
		this.date = date;
	}
	
	public Twit(Cursor result)
	{
		this.body = result.getString(1);
		this.owner = result.getString(2);
		this.date = new Date(result.getString(3));
	}
}
