package com.example.twitterfacebookbridge.sys.twitter;

/*
 * sources get from https://github.com/ddewaele/AndroidTwitterGoogleApiJavaClient
 * */

import java.util.List;

import com.example.twitterfacebookbridge.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TwitArrayAdapter extends ArrayAdapter<Twit> {
	
    Context context;
    
    public TwitArrayAdapter(Context context, int resourceId, List<Twit> twits) {
        super(context, resourceId, twits);
        this.context = context;
    }
 
    public View getView(int position, View convertView, ViewGroup parent) {
    	Twit rowItem = getItem(position);
        TextView bodyTxtView;
        TextView ownerTxtView;
        TextView dateTxtView;
        
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_twit_item, null);
            bodyTxtView = (TextView) convertView.findViewById(R.id.bodyLabel);
            ownerTxtView = (TextView) convertView.findViewById(R.id.ownerLabel);
            dateTxtView = (TextView) convertView.findViewById(R.id.dateLabel);
            
            bodyTxtView.setText(rowItem.body);
            ownerTxtView.setText(rowItem.owner);
            dateTxtView.setText(rowItem.date.toGMTString());
            //txtView.setCompoundDrawablesWithIntrinsicBounds(rowItem.imgId, 0, R.drawable.forward, 0);
        } else {
        	// do nothing
        }
        
        return convertView;
    }
}
