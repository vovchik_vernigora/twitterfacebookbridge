package com.example.twitterfacebookbridge;

/*
 * this is fragment which contains all twits saved in db
 * 
 * @author vofan
 * */

import java.util.ArrayList;
import java.util.List;
import com.example.twitterfacebookbridge.sys.DB.DBManager;
import com.example.twitterfacebookbridge.sys.twitter.Twit;
import com.example.twitterfacebookbridge.sys.twitter.TwitArrayAdapter;
import android.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class TwitsHolderFragment extends Fragment
	implements SwipeRefreshLayout.OnRefreshListener{

	SwipeRefreshLayout mSwipeRefreshLayout;
	ListView listView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		return rootView;
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onActivityCreated(Bundle savedInstanceStat) {
		super.onActivityCreated(savedInstanceStat);

		mSwipeRefreshLayout = (SwipeRefreshLayout) getActivity().findViewById(
				R.id.refresh);
		mSwipeRefreshLayout.setOnRefreshListener(this);

		
		 mSwipeRefreshLayout.setColorScheme(R.color.blue, R.color.green,
		 R.color.orange, R.color.red);

		listView = (ListView) getActivity().findViewById(android.R.id.list);
		
		new FillListFromDBATask().execute();

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Twit selected = ((Twit) listView.getItemAtPosition(position)); 

				Intent intent = new Intent(getActivity(), Publisher.class);
				Bundle extras = new Bundle();
				extras.putString("body", selected.body);
				extras.putString("owner", selected.owner);
				extras.putString("date", selected.date.toGMTString());
				
				intent.putExtras(extras);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onRefresh() {
		Toast.makeText(getActivity(), R.string.refresh_started,
				Toast.LENGTH_SHORT).show();
		
		new FillListFromDBATask().execute();
	}
	
	private class FillListFromDBATask extends AsyncTask<Void, Void, List<Twit>> {
		protected List<Twit> doInBackground(Void... params) {
			
			mSwipeRefreshLayout.setRefreshing(true);
			
			List<Twit> items = new ArrayList<Twit>();
			Cursor curcorResult;
			try {
				curcorResult = DBManager.readTwitsFromDB();
				
				curcorResult.moveToFirst();
				while (!curcorResult.isAfterLast()) {
					items.add(new Twit(curcorResult));
					curcorResult.moveToNext();
				}
				curcorResult.close();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return items;
		}

		protected void onPostExecute(List<Twit> items) {
			mSwipeRefreshLayout.setRefreshing(false);
			
			listView.setAdapter(new TwitArrayAdapter(getActivity(),
					android.R.layout.simple_list_item_1, items));
		}
	}
}
