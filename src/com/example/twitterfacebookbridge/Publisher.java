package com.example.twitterfacebookbridge;

/*
 * This is activity that represents twit which user plan to post to facebook wall
 * 
 * @author vofan
 * */

import java.util.Date;

import com.example.twitterfacebookbridge.sys.facebook.FBProvider;
import com.example.twitterfacebookbridge.sys.twitter.Twit;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Publisher extends Activity {

	public final static int FILE_SELECT_CODE = 0;
	
	Twit twitToPublish;
	
	TextView bodyTxt, ownerTxt, dateTxt;
	Button publishBtn;
	Button loadImgBtn;
	ImageView imgPreview;
	
	Uri selectedImg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_publisher);
		
		Bundle extras = getIntent().getExtras();
		
		twitToPublish = new Twit(extras.getString("body"),
				extras.getString("owner"), 
				new Date(extras.getString("date")));
		
		bodyTxt = (TextView)findViewById(R.id.publishBody);
		bodyTxt.setText(twitToPublish.body);
		
		ownerTxt = (TextView)findViewById(R.id.publishOwner);
		ownerTxt.setText(twitToPublish.owner);
		
		dateTxt = (TextView)findViewById(R.id.publishDate);
		dateTxt.setText(twitToPublish.date.toGMTString());
		
		publishBtn = (Button)findViewById(R.id.publishBtn);
		loadImgBtn = (Button)findViewById(R.id.publishLoadImgBtn);
		imgPreview = (ImageView)findViewById(R.id.publishImageView);
		
		publishBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(MainActivity.isNetworkConnected()){
					if(selectedImg != null){
						FBProvider.postOnWall(selectedImg, twitToPublish.body);
					}
					else 
					{
						Toast.makeText(Publisher.this, "select image first", Toast.LENGTH_SHORT).show();
					}
				}
				else
				{
					Toast.makeText(Publisher.this, "no internet connection", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		loadImgBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				showFileChooser();
			}
		});
		
		FBProvider.Init(this, savedInstanceState);
	}
	
	private void showFileChooser() {
		Intent intent = new Intent(Intent.ACTION_GET_CONTENT,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		try {
			startActivityForResult(
					Intent.createChooser(intent, "Select a File to Upload"),
					FILE_SELECT_CODE);
		} catch (android.content.ActivityNotFoundException ex) {
			Toast.makeText(this, "Please install a File Manager.",
					Toast.LENGTH_SHORT).show();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {
		case FILE_SELECT_CODE:
			if (resultCode == RESULT_OK) {
				selectedImg = data.getData();
				Toast.makeText(this, "File Uri: " + selectedImg.toString(),
						Toast.LENGTH_SHORT).show();
				imgPreview.setImageURI(selectedImg);
			}
			break;
		}
		FBProvider.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		FBProvider.onResume();
	}
	@Override
	public void onPause() {
		super.onPause();
		FBProvider.onPause();
	}
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		FBProvider.onSaveInstanceState(outState);
	}
	@Override
	public void onDestroy() {
		super.onDestroy();
		FBProvider.onDestroy();
	}
}
