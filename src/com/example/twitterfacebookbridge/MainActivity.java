package com.example.twitterfacebookbridge;

/*
 * Main activity class. income point of app
 * 
 * @author vofan
 * */

import java.util.ArrayList;
import java.util.List;

import twitter4j.ResponseList;

import com.example.twitterfacebookbridge.sys.DB.DBManager;
import com.example.twitterfacebookbridge.sys.twitter.Twit;
import com.example.twitterfacebookbridge.sys.twitter.TwitterUtils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends Activity {

	public static final String TAG = MainActivity.class.getName();
	public static final String PERSON_TO_SEARCH = "ladygaga";
	private TwitsHolderFragment twitsFragment;
	public static SharedPreferences prefs;
	private static ConnectivityManager cm;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		if (savedInstanceState == null) {
			twitsFragment = new TwitsHolderFragment();
			getFragmentManager().beginTransaction()
					.add(R.id.container, twitsFragment).commit();
		}
		
		DBManager.init(getApplicationContext());
		
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id) {
		case R.id.action_update: {
			new ApiCallExecutor().execute(PERSON_TO_SEARCH);
			return true;
		}
		case R.id.action_connectTwitter: {
			if (isNetworkConnected()){
				new Thread(){//prevent "network in UI thread" exception
					public void run()
					{
						if(!TwitterUtils.isAuthenticated(prefs)){
	
								startActivity(new Intent().setClass(MainActivity.this,OAuthAccessTokenActivity.class));
	
						}
						else 
						{
							new ApiCallExecutor().execute(PERSON_TO_SEARCH);
						}
					}
				}.start();
			}
			else 
			{
				Toast.makeText(MainActivity.this, "no internet connection", Toast.LENGTH_SHORT).show();
			}
			return true;
		}
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if (isNetworkConnected()){
			new ApiCallExecutor().execute(PERSON_TO_SEARCH);
		}
	}
	
	/**
	 * loads timeline from selected person
	 * */
	private class ApiCallExecutor extends AsyncTask<String, Void, Void> {
		@Override
		protected Void doInBackground(String... params) {
			if(params.length > 0){
				try {
					ResponseList<twitter4j.Status> remoteTimeline = TwitterUtils.getUserTimeline(prefs, params[0],10);
					List<twitter4j.Status> statusList = new ArrayList<twitter4j.Status>();
					for (twitter4j.Status status : remoteTimeline) {
						statusList.add(status);
						DBManager.insertTwit(new Twit(status.getText(), status.getUser().getName(), status.getCreatedAt()));
						Log.d(TAG,status.getText());
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			twitsFragment.onRefresh();
		}
	}
	
	public static boolean isNetworkConnected() {
		NetworkInfo ni = cm.getActiveNetworkInfo();
		if (ni == null) {
			return false;
		} else
			return true;
	}
}
